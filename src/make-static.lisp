(let* ((static-libs (mapcar #'file-namestring (directory "libeql5_*.a")))
       (mri (format nil "create libeql5static.a~%addlib libini_eql5.a~%addlib ../libeql5.a~%~{addlib ~a~%~}save~%end~%" static-libs)))
  (ext:system (format nil "sh -c 'echo ~s | ar -M && mv libeql5static.a ../libeql5.a'" mri)))
